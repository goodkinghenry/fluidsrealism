/*
 * FluidsRealism
 * This plugin dramatically improves water behavior in Minecraft servers with Spigot-compatible APIs.
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.fluidsrealism;

import static digital.amaranth.mc.quickblocklib.Plugins.Plugins.getInstanceByName;

import org.bukkit.event.HandlerList;

import org.bukkit.plugin.java.JavaPlugin;

public class FluidsRealism extends JavaPlugin {
    public static int SUCTION_SEARCH_MAX_COST;
    public static int PRESSURE_SEARCH_MAX_COST;
    
    private void loadConfiguredVariables () {
        SUCTION_SEARCH_MAX_COST    = getConfig().getInt("path-finding.suction-search-max-cost");
        PRESSURE_SEARCH_MAX_COST   = getConfig().getInt("path-finding.pressure-search-max-cost");
    }
    
    public static FluidsRealism getPlugin () {
        return (FluidsRealism)getInstanceByName("FluidsRealism");
    }
    
    @Override
    public void onEnable() {
        saveDefaultConfig();
        loadConfiguredVariables();
        
        getServer().getPluginManager().registerEvents(new FluidFlow(), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
