/*
 * FluidsRealism
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.fluidsrealism.pathfinders;

import digital.amaranth.mc.dynamicpaths.Pathfinders.DepthFirst.DepthFirst;

import static digital.amaranth.mc.fluidsrealism.FluidsRealism.PRESSURE_SEARCH_MAX_COST;

import java.util.Arrays;

import org.bukkit.block.Block;

import static digital.amaranth.mc.quickblocklib.Blocks.Conditions.areSameType;
import static digital.amaranth.mc.quickblocklib.Blocks.Conditions.isHigherThan;

import static digital.amaranth.mc.quickblocklib.Neighbors.Groups.ALL_DIRECT_FACES;

import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_FLOWING_FLUID_LEVEL;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.canFlowToward;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.getFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isAFlowingFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isASourceBlock;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isFullerThan;

public class LowerNonSourceBlockFinder extends DepthFirst {
    @Override
    public boolean matchCondition (Block b) {
        return isHigherThan(getFirst(), b) && !isASourceBlock(b);
    }
    
    @Override
    public boolean breakCondition (Block b) {
        return hasAMatchedBlock() && !isAFlowingFluidLevel(getFluidLevel(b).orElse(LOWEST_FLOWING_FLUID_LEVEL) + 1);
    }
    
    @Override
    public boolean canTraverse (Block to, Block from) {
        return  !hasTraversed(to) && areSameType(from, getFirst()) 
                && (areSameType(to, getFirst()) || (isHigherThan(getFirst(), to) && canFlowToward(to, getFirst())));
    }
    
    @Override
    public int traversalCost (Block to, Block from) {
        int v = (Integer.compare(to.getY(), from.getY()));
        
        if (v >= 0) {
            if (isFullerThan(to, from)) {
                return countTraversableNeighbors(to);
            } else {
                return countTraversableNeighbors(to) - 1;
            }
        } else {
            return 0;
        }
    }
    
    public LowerNonSourceBlockFinder (Block first) { 
        super(first, Arrays.asList(ALL_DIRECT_FACES), 0, PRESSURE_SEARCH_MAX_COST);
        
        search();
    }
}
