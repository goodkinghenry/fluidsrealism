/*
 * FluidsRealism
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.fluidsrealism.pathfinders;

import digital.amaranth.mc.dynamicpaths.Pathfinders.DepthFirst.DepthFirst;

import java.util.Arrays;

import static digital.amaranth.mc.quickblocklib.Blocks.Conditions.areSameType;
import static digital.amaranth.mc.quickblocklib.Blocks.Conditions.isHigherThan;

import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isFullerThan;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isASourceBlock;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.canBeFlowedToward;

import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_FLOWING_WATER_LEVEL;

import static digital.amaranth.mc.quickblocklib.Neighbors.Groups.ALL_DIRECT_FACES_EXCEPT_DOWN;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public class SourceBlockFinder extends DepthFirst {
    @Override
    public boolean matchCondition (Block b) {
        return isASourceBlock(b);
    }
    
    @Override
    public boolean breakCondition (Block b) {
        return hasAMatchedBlock() && canBeFlowedToward(b.getRelative(BlockFace.UP));
    }
    
    @Override
    public boolean canTraverse (Block to, Block from) {
        return !hasTraversed(to) && areSameType(to, getFirst()) && !isHigherThan(from, to);
    }
    
    @Override
    public int traversalCost (Block to, Block from) {
        if (to.getY() > from.getY() || isFullerThan(to, from)) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public SourceBlockFinder (Block first) { 
        super(first, Arrays.asList(ALL_DIRECT_FACES_EXCEPT_DOWN), 0, LOWEST_FLOWING_WATER_LEVEL + 1);
        
        search();
    }
}
