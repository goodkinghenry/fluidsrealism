/*
 * FluidsRealism
 * realistic fluid behavior!
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.fluidsrealism;

import static digital.amaranth.mc.fluidsrealism.FluidsRealism.getPlugin;

import digital.amaranth.mc.fluidsrealism.pathfinders.LowerNonSourceBlockFinder;
import digital.amaranth.mc.fluidsrealism.pathfinders.SourceBlockFinder;

import static digital.amaranth.mc.quickblocklib.Blocks.Changes.copyTo;
import static digital.amaranth.mc.quickblocklib.Blocks.Changes.removeBlock;
import static digital.amaranth.mc.quickblocklib.Blocks.Changes.transferTo;

import static digital.amaranth.mc.quickblocklib.Fluids.Constants.TICKS_PER_FLUID_UPDATE;

import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isAFluidBlock;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isAWaterBlock;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isAFallingFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isAFlowingFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.isASourceFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.getFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.setFluidLevel;
import static digital.amaranth.mc.quickblocklib.Fluids.Fluids.canFlowToward;

import static digital.amaranth.mc.quickblocklib.Weather.Conditions.isInRain;

import static digital.amaranth.mc.quickblocklib.Neighbors.Neighbors.getDirectAndDiagonalNeighbors;
import static digital.amaranth.mc.quickblocklib.Neighbors.Neighbors.getDirectNeighborsExceptUp;

import org.bukkit.Bukkit;

import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.FluidLevelChangeEvent;
import org.bukkit.event.block.MoistureChangeEvent;

import org.bukkit.event.player.PlayerBucketEmptyEvent;

public class FluidFlow implements Listener {
    private boolean flowToward (Block to, Block from) {
        if (!canFlowToward(to, from)) {
            return false;
        } else {
            return getFluidLevel(from).stream().
                filter(l -> isAFlowingFluidLevel(l + 1)).
                anyMatch(l -> {
                    copyTo(to, from);

                    return setFluidLevel(to, l + 1);
                });
        }
    }
    
    private boolean flowToLowerBlock (Block b) {
        if (!isAFluidBlock(b)) {
            return false;
        } else {
            return new SourceBlockFinder(b).getMatchedBlocks().
                    stream().
                    sorted((mb1, mb2) -> Integer.compare(mb2.getY(), mb1.getY())).
                    anyMatch(sb -> new LowerNonSourceBlockFinder(b).getMatchedBlocks().
                            stream().
                            sorted((mb1, mb2) -> Integer.compare(mb1.getY(), mb2.getY())).
                            anyMatch(lb -> {
                                transferTo(lb, sb);

                                scheduleFlow(lb);

                                return true;
                            })
                    );
        }
    }
    
    private void scheduleFlow (Block b) {
        Bukkit.getScheduler().runTaskLater(getPlugin(), () -> {
            flowFrom(b);
        }, TICKS_PER_FLUID_UPDATE);
    }
    
    private void flowFrom (Block b) {
        if (!flowToLowerBlock(b) && new SourceBlockFinder(b).hasAMatchedBlock()) {
            getDirectNeighborsExceptUp(b).stream().
                    anyMatch(n -> flowToward(n, b) && flowToLowerBlock(b));
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockFromToEvent (BlockFromToEvent e) {
        if (isAFluidBlock(e.getBlock())) {
            e.setCancelled(true);
            
            flowFrom(e.getBlock());
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onFluidLevelChangeEvent (FluidLevelChangeEvent e) {
        if (e.getNewData() instanceof Levelled nl) {
            if (isASourceFluidLevel(nl.getLevel()) || isAFallingFluidLevel(nl.getLevel())) {
                e.setCancelled(true);
            }
            
            flowFrom(e.getBlock());
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onMoistureChangeEvent (MoistureChangeEvent e) {
        if (!isInRain(e.getBlock())) {
            getDirectAndDiagonalNeighbors(e.getBlock()).stream().
                    filter(n -> isAWaterBlock(n)).
                    findAny().ifPresent(n -> 
                            new SourceBlockFinder(n).getBestMatchByCost().ifPresent(sb -> removeBlock(sb))
                    );
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onPlayerBucketEmptyEvent (PlayerBucketEmptyEvent e) {
        scheduleFlow(e.getBlock());
    }
}
