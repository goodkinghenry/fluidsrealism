I've finally rewritten FluidsRealism, and it now behaves the way that I had originally set out for it to. I hope that you will enjoy it! Under the hood, it uses two of my own libraries, DynamicPaths and QuickBlockLib. They are packaged in with the jar at build time, so only one file will need to be installed under the plugins folder; however, if you would like to link to them in your own project, or study and modify them, view their sources here:

https://git.amaranth.digital/bproffit/mc.quickblocklib

https://git.amaranth.digital/bproffit/mc.dynamicpaths

Along with some cool path-finding algorithms (co-authored with my sister, Breeana Bellflower), there is one other neat trick: water will be absorbed as it moistens a farmland block.

Water is finite and flows realistically. What more could you want?

Please let me know when you find bugs so I can fix them, and as always: use at your own risk, particularly in a production environment.

Bradley Proffit